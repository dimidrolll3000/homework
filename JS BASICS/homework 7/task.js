function getList(array) {

    let ul = document.createElement('ul');
    document.body.append(ul);
  
    let newArray = array.map( item => item = `<li>${item}</li>`);
  
    ul.innerHTML = newArray.join('');
  }
  
  console.log(getList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']));