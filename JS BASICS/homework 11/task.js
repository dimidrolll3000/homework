const buttons = document.querySelectorAll('.btn');
const box = document.querySelector('body');

box.addEventListener('keydown', onKeyPress);

function onKeyPress(event) {
    buttons.forEach( btn => btn.classList.contains('active')? btn.classList.remove('active') : false);
    buttons.forEach( btn => event.key.toUpperCase() === btn.textContent.toUpperCase()? btn.classList.add('active') : false);
}