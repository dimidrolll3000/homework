let liList = document.querySelector('.tabs').querySelectorAll('li');
let pList = document.querySelector('.tabs-content').querySelectorAll('li');

for (let i=0; i < liList.length; i++) {
  liList[i].classList.add(liList[i].textContent.toLowerCase());
  pList[i].classList.add(liList[i].textContent.toLowerCase()); 
 !liList[i].classList.contains('active')? pList[i].style.display = 'none' : false; 
}

let tabs = document.querySelector('.tabs');
tabs.addEventListener('click', onClick)

function onClick(event) {
    if (event.target.tagName !== 'LI') return;
    
    Array.from(tabs.children).forEach( li => li.classList.remove('active'));
    event.target.classList.add('active');

    pList.forEach( p => {

        if (p.classList.contains(event.target.textContent.toLowerCase())) {
           return p.style.display = '';
        } else {
            return p.style.display = 'none';
        }
    });
}
