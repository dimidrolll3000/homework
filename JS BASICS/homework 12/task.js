const imgs = document.querySelectorAll('img');
const pause = document.createElement('button');
const continuee = document.createElement('button');
let timer;

pause.textContent = 'Прекратить';
continuee.textContent = 'Возобновить показ';
document.body.append(pause);
document.body.append(continuee);

imgs.forEach( (item, index)=> index? item.style.display='none': false );

let i = 0;

function start () {
    timer = setTimeout( function slider() {
        imgs[i].style.display = 'none';
        (!imgs[i+1]) ? imgs[0].style.display = 'inline': imgs[i+1].style.display = 'inline';

        i++;
        if (i === imgs.length) i=0;
        
        timer = setTimeout(slider,10000)
    }, 10000);
};
start();

document.addEventListener('click', function onClick(event) {
    if (event.target === pause) {
      clearTimeout(timer); 
      timer = 0; 
    }
    if (event.target === continuee && !timer) start();
});
