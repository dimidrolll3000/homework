const notAllowedString = 'You are not allowed to visit this website';
const welcomeString = 'Welcome';

let userName = prompt('Enter your name please')
while (!isNaN(userName) || userName === false) {
    userName = prompt('Enter your name again please' ,userName)
}

let displayUserAge = prompt('Enter your age please')
let userAge = +displayUserAge
while (isNaN(userAge) || userAge === 0) {
    displayUserAge = prompt('Enter your age again please' ,displayUserAge)
    userAge = +displayUserAge
}

if (displayUserAge < 18) {
    alert(notAllowedString)
}
else if (displayUserAge >= 18 && displayUserAge <= 22) {
    let answer = confirm('Are you sure you want to continue?')
    if (answer == true) {
        alert(`${welcomeString}, ${userName}`)
    }
    else if (answer == false) {
        alert(notAllowedString)
    }
}
else if (displayUserAge > 22) {
    alert(`${welcomeString}, ${userName}`)
}