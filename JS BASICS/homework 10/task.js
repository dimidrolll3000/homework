let confirmation = document.querySelector('.fa-eye-slash');
confirmation.classList.add('fa-eye');
confirmation.classList.remove('fa-eye-slash');

let inputs = document.querySelectorAll('input');

let form = document.querySelector('form');
form.classList.add('confirmation-box');

form.addEventListener('mousedown', getIcon);
form.addEventListener('mouseup', getIcon);

function getIcon(event) {
    if (event.target.tagName !== 'I') return;
    event.target.classList.toggle('fa-eye-slash');
    event.target.classList.toggle('fa-eye');

    let input = event.target.previousElementSibling;
    
    if (input.getAttribute('type') === 'password' ) {
        input.setAttribute('type', 'text')
    } else {
        input.setAttribute('type', 'password')
    }
}
form.addEventListener('submit', onSubmit);

function onSubmit(event) {
    event.preventDefault();

    let arr = [];
    for (let input of inputs) {
        arr.push(input.value)
    }

    if (arr[0] === arr[1] && arr[0]) {
        alert('You are welcome');
    } else if (!document.querySelector('.error')) {
        let span = document.createElement('span');
        span.classList.add('error');
        span.textContent = 'Нужно ввести одинаковые значения';
        inputs[1].nextElementSibling.after(span);
    }
}

form.addEventListener('focus', function (e) {
    let error = document.querySelector('.error');
    if (e.target.tagName !== 'INPUT' || !error) return;
    error.remove()
}, true);