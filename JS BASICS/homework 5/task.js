let firstName = prompt("Введите своё имя");
let lastName = prompt("Введите свою фамилию");
let birthday = prompt("Ввежите дату рождения (в формате: дд.мм.гггг)");


function createNewUser() {
  return {
    firstName,
    lastName,
    birthday,

    getAge() {
      if (this.birthday !== null) {
        let array = this.birthday.split('.');
        let passedTime = new Date() - new Date(array.reverse().join('.'));
        return `Your age is ${Math.floor(passedTime/1000/60/60/24/365)}.`;
      }
      
    },

    getLogin: function() {
        return (this.firstName.substr(0, 1)).toLowerCase() + this.lastName.toLowerCase()
    },

    getPassword() {
      if (this.birthday !== null) {
        let array = this.birthday.split('.');
        return `Предлагаемый пароль '${(this.firstName.substr(0, 1)).toLowerCase() + this.lastName.toLowerCase() + array[2]}'` 
      }
    },
  }
}
console.log(createNewUser());
console.log(createNewUser().getAge());
console.log(createNewUser().getPassword());