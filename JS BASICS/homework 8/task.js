window.addEventListener('load', setForm);

function setForm() {

  let form = document.createElement('form');
  form.setAttribute('action', '#void');
  document.body.append(form);

  let input = document.createElement('input');
  input.setAttribute('id', 'relatedInput');
  form.append(input);
  form.style.cssText = `
    position: absolute;
    top: 35px;`;

  let label = document.createElement('label');
  label.textContent = 'Price: ';
  label.setAttribute('for', 'relatedInput');
  form.prepend(label);

  document.addEventListener('focus', getFocused, true);

  function getFocused(event) {

    if (event.target.tagName === 'INPUT') {
      input.style.cssText = `
        border: 2px solid green;
        outline: none;
      `;
    }

    let error = document.querySelector('.error');
    (error) ? error.remove() : false;
  }

  document.addEventListener('keydown', getDigits, true);

  function getDigits(event) {
    if (!Number.isFinite(+event.key) 
        && event.key !== 'Backspace' 
        && event.key !== 'Tab' 
        && event.key !== '-') {
      event.preventDefault();
    } 
  }

  document.addEventListener('blur', getFocusOut, true);

  function getFocusOut() {
    input.style.cssText = '';

    if (input.value > 0 ) {
      input.style.color = 'green';

      if (document.querySelectorAll('.current-price').length <= 0) {
        let span = document.createElement('span');
        span.classList.add('current-price');
        document.body.prepend(span);
      } 

      let span = document.querySelector('.current-price');
      span.innerHTML = `Текущая цена: ${input.value} <span>x</span>`;
      span.style.cssText = `
          border-radius: 50px;
          color: darkgrey;
          padding: 5px;
          border: 1px solid lightgrey;
        `;    

      let X = document.querySelectorAll('span')[1];
      X.classList.add('x');
      X.style.cssText = `
        border: 1px solid darkgrey;
        border-radius: 50%;
        font-family: Arial;
        font-size: 11px;
        padding: 0 4px 2px 4px;
        vertical-align: top;
      `;

      document.addEventListener('click', onClick);

      function onClick(event) {
        if (event.target === X) {
          span.style.display = 'none';
          input.value = '';
        }
      }

    } else if (input.value < 0) {
      input.style.borderColor = 'red';

      let error = document.createElement('span');
      error.classList.add('error');
      error.style.cssText = `
        font-style: italic;
        color: red;
        display: block;
      `;
      error.textContent = `Please enter correct price`;

      let span = document.querySelector('.current-price');
      (span) ? span.remove() : false;

      if (form.querySelectorAll('.error').length <= 0) {
        form.append(error);
      } 
    }  
  }

}