const mainBox = document.querySelector('.body');
const links = Array.from(document.querySelectorAll('a'));

document.addEventListener('DOMContentLoaded', () => {

    document.addEventListener('click', setTheme);
    function setTheme(event) {
        
        if(!event.target.classList.contains('toggler')) return;
        event.preventDefault();
    
        mainBox.classList.toggle('pink-theme');
        links.forEach(li => li.classList.toggle('pink-theme'));  
        
        const theme = mainBox.classList[1];
        (theme)? localStorage.setItem('theme', theme) : localStorage.setItem('theme', '');
    }
    
        if (localStorage.getItem('theme')) {
        mainBox.classList.add(localStorage.getItem('theme'));
        links.forEach(li => li.classList.add(localStorage.getItem('theme')));
    }
});