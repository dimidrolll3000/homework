let liList = document.querySelector('.tabs').querySelectorAll('li');
let pList = document.querySelector('.tabs-content').querySelectorAll('li');

for (let i=0; i < liList.length; i++) {
  liList[i].classList.add(liList[i].textContent.replace(' ', '-').toLowerCase());
  pList[i].classList.add(liList[i].textContent.replace(' ', '-').toLowerCase()); 
 !liList[i].classList.contains('active')? pList[i].style.display = 'none' : false; 
}

let tabs = document.querySelector('.tabs');
tabs.addEventListener('click', onClick)

function onClick(event) {
    if (event.target.tagName !== 'LI') return;
    
    Array.from(tabs.children).forEach( li => li.classList.remove('active'));
    event.target.classList.add('active');

    pList.forEach( p => {

        if (p.classList.contains(event.target.textContent.replace(' ', '-').toLowerCase())) {
           return p.style.display = '';
        } else {
            return p.style.display = 'none';
        }
    });
}

const hoverBlocks = {
	graphic: `<div class="hover-block">
        <div class="hover-block-svg">
            <div class="hover-block-svg-wrapper">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759143 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922616 9.8266 0.0883684 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                </svg>
            </div>
            <div class="hover-block-svg-wrapper">
                <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="12" height="11" fill="#18CFAB"/>
                </svg>
            </div>
        </div>
        <h2>creative design</h2>
        <p>graphic design</p>
    </div>`,
	design: `<div class="hover-block">
        <div class="hover-block-svg">
            <div class="hover-block-svg-wrapper">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759143 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922616 9.8266 0.0883684 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                </svg>
            </div>
            <div class="hover-block-svg-wrapper">
                <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="12" height="11" fill="#18CFAB"/>
                </svg>
            </div>
        </div>
        <h2>creative design</h2>
        <p>web design</p>
    </div>`,
    landing: `<div class="hover-block">
        <div class="hover-block-svg">
            <div class="hover-block-svg-wrapper">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759143 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922616 9.8266 0.0883684 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                </svg>
            </div>
            <div class="hover-block-svg-wrapper">
                <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="12" height="11" fill="#18CFAB"/>
                </svg>
            </div>
        </div>
        <h2>creative design</h2>
        <p>landing page</p>
    </div>`,
	wordpress: `<div class="hover-block">
        <div class="hover-block-svg">
            <div class="hover-block-svg-wrapper">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759143 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922616 9.8266 0.0883684 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                </svg>
            </div>
            <div class="hover-block-svg-wrapper">
                <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="12" height="11" fill="#18CFAB"/>
                </svg>
            </div>
        </div>
        <h2>creative design</h2>
        <p>wordpress</p>
    </div>`,
}
const root = document.getElementById('root')

for (let i = 1; i <= 3; i++) {
	root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item" id="graphic">
            ${hoverBlocks.graphic}
            <img src="./images/graphic-design/graphic-design${i}.jpg" alt="">
        </div>`,
	)
	root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item" id="design">
            ${hoverBlocks.design}
            <img src="./images/web-design/web-design${i}.jpg" alt="">
        </div>`,
	)
    root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item" id="landing">
            ${hoverBlocks.landing}
            <img src="./images/landing-page/landing-page${i}.jpg" alt="">
        </div>`,
	)
	root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item" id="wordpress">
            ${hoverBlocks.wordpress}
            <img src="./images/wordpress/wordpress${i}.jpg" alt="">
        </div>`,
	)
}
for (let i = 4; i <= 6; i++) {
	root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item hidden" id="graphic">
            ${hoverBlocks.graphic}
            <img src="./images/graphic-design/graphic-design${i}.jpg" alt="">
        </div>`,
	)
	root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item hidden" id="design">
            ${hoverBlocks.design}
            <img src="./images/web-design/web-design${i}.jpg" alt="">
        </div>`,
	)
    root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item hidden" id="landing">
            ${hoverBlocks.landing}
            <img src="./images/landing-page/landing-page${i}.jpg" alt="">
        </div>`,
	)
	root.insertAdjacentHTML(
		'afterbegin',
		`<div class="root-item hidden" id="wordpress">
            ${hoverBlocks.wordpress}
            <img src="./images/wordpress/wordpress${i}.jpg" alt="">
        </div>`,
	)
}

document.getElementById('loadmore').addEventListener('click', (event) => {
    const allHiddenImgs = document.querySelectorAll('.hidden')
    
    allHiddenImgs.forEach(singleImg => {
        singleImg.classList.remove('hidden')
    })
    event.target.classList.add('hidden')
})

const amazingTabs = document.querySelector('.amazing-tabs');
const showAllTab = document.querySelector('.show-all')
const graphicTab = document.querySelector('.graphic')
const designTab =  document.querySelector('.design')
const landingTab = document.querySelector('.landing')
const wordpressTab = document.querySelector('.wordpress')

const allFilteredDivs = document.querySelectorAll('.root-item')

const allFilteredDivsArray = Array.from(allFilteredDivs)

showAllTab.addEventListener('click', showAll)
function showAll(event) {
  allFilteredDivsArray.forEach(div => {
    div.classList.remove('filtred')
    div.classList.add('show')
    Array.from(amazingTabs.children).forEach( li => li.classList.remove('amazing-active'));
    event.target.classList.add('amazing-active');
  })
}

graphicTab.addEventListener('click', showGraphic)
function showGraphic(event) {
  allFilteredDivsArray.forEach(div => {
    div.classList.remove('filtred')

    if (div.id != 'graphic') {
      div.classList.add('filtred')
    }

    Array.from(amazingTabs.children).forEach( li => li.classList.remove('amazing-active'));
    event.target.classList.add('amazing-active');
  })
}

designTab.addEventListener('click', showDesign)
function showDesign(event) {
  allFilteredDivsArray.forEach(div => {
    div.classList.remove('filtred')

    if (div.id != 'design') {
      div.classList.add('filtred')
    }

    Array.from(amazingTabs.children).forEach( li => li.classList.remove('amazing-active'));
    event.target.classList.add('amazing-active');
  })
}

landingTab.addEventListener('click', showLanding)
function showLanding(event) {
  allFilteredDivsArray.forEach(div => {
    div.classList.remove('filtred')
    
    if (div.id != 'landing') {
      div.classList.add('filtred')
    }

    Array.from(amazingTabs.children).forEach( li => li.classList.remove('amazing-active'));
    event.target.classList.add('amazing-active');
  })
}

wordpressTab.addEventListener('click', showWordpress)
function showWordpress(event) {
  allFilteredDivsArray.forEach(div => {
    div.classList.remove('filtred')
    
    if (div.id != 'wordpress') {
      div.classList.add('filtred')
    }

    Array.from(amazingTabs.children).forEach( li => li.classList.remove('amazing-active'));
    event.target.classList.add('amazing-active');
  })
}

$(document).ready(function(){
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        infinite: true,
        centerMode: true,
        asNavFor: '.slider-nav'
      });
      $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        asNavFor: '.slider-for',
        focusOnSelect: true,
        prevArrow: `<div class='a-left control-c prev slick-prev'>
                <svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M6.50764 12.5076L7.81261 11.1056L3.52463 6.49631L7.81261 1.88987L6.50764 0.486695L0.91457 6.49631L6.50764 12.5076Z" fill="#BBBBBB"/>
                </svg>        
            </div>`,
        nextArrow: `<div class='a-right control-c next slick-next'>
                <svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.49213 0.48752L0.18704 1.88987L4.47503 6.49714L0.18704 11.1056L1.49213 12.5084L7.08508 6.49714L1.49213 0.48752Z" fill="#BBBBBB"/>
                </svg>        
            </div>`
      });
  });
